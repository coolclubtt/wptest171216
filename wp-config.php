<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpressuser');

/** MySQL database password */
define('DB_PASSWORD', '0000');
define('FS_METHOD', 'direct');
/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         '((PYN,Qf:4N/~yrSe(iEMBJ$g@Vg*bm%@e`,+|J`>v,l*+yu=H2#M!m.j#5wX9Pr');
define('SECURE_AUTH_KEY',  'Zjo;0_oVA(EzA_BkB[:-)KxI8@2c+>x7[YDoxX!riRMVB|?A.;.do2?E7H|A|-Yg');
define('LOGGED_IN_KEY',    '!OMui_5ycj=G&ckAWNzcL[Ug-=j]6ld28Sgy1yye53*h#.-`BeYGzZZA-z*p-z_T');
define('NONCE_KEY',        '!4`?DU2xeF8[:`@$i0+gElMP?gU{#t!xJr.Fo6^UgdHhQgApq_BCwv)3*gSoG,&P');
define('AUTH_SALT',        'T*V(IN8KlL|7i;v A|6<RXoh+G,<m$6]%#zXJT#bX*1C>) %ZU9j]6X5|%ca/<MK');
define('SECURE_AUTH_SALT', '$hvdp*!+{i-y?t=C?%+VJ{^J-cKbIG)+(I7bg[ <Rw+86/ND;Kjh4zrNs-4@=@(t');
define('LOGGED_IN_SALT',   'X-+Gb[+qC umRiHIe8O%.DqIF-dOLb{?JHn{nX;KeaQr}Rx`U1S%|nl6_8idOXNV');
define('NONCE_SALT',       'Zu[|R8^Gc1*QO[D6F:EKX+u+Y]2!E:TOf-t#yNY9<vv~P?IIASU.NHk-ml}n}=`;');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
